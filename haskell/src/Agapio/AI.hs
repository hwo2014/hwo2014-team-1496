
-- AI logic for the car
module Agapio.AI where

import Control.Monad.State

import Agapio.Client.Monad
import Agapio.AI.State
import Agapio.AI.Track (AnalyzedTrack)
import Agapio.AI.Metrics
import Agapio.Protocol
import Agapio.Types
import Agapio.Models

statefulHandler :: Track -> CarId -> AnalyzedTrack -> Metrics -> GameState -> (Track -> CarId -> AnalyzedTrack -> GameState -> ServerMessage -> State Metrics ClientMessage) -> Client ()
statefulHandler track myId curves metrics initial handler = step metrics initial where
    step prevMetrics prevState = do
        msg <- fromServer
        case msg of
            CarPositions dat _ ->
                let state = updater dat prevState
                    (resp, metrics')  = runState (handler' state msg) prevMetrics
                in  sendMessage resp >> step metrics' state

            TurboAvailable _ _ ->
                let (resp, metrics')  = runState (handler' prevState msg) prevMetrics
                in  sendMessage resp >> step metrics' prevState

            _ ->
                let (resp, metrics') = runState (handler' prevState msg) prevMetrics
                in  sendMessage resp >> step metrics' prevState

    updater  = updateState track
    handler' = handler track myId curves


