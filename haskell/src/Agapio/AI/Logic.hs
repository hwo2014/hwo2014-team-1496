{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedStrings #-}

module Agapio.AI.Logic where

import qualified Data.Text as T

import Control.Monad.State
import Data.Array (bounds, (!), assocs)
import Data.List (find, minimumBy)
import Data.Ord (comparing)
import Data.Maybe (isJust)

import Agapio.AI.State
import Agapio.AI.Metrics
import Agapio.Types
import Agapio.Models
import Agapio.Protocol
import Agapio.AI.Track

import Debug.Trace
import Text.Printf

logic :: Track -> CarId -> AnalyzedTrack -> GameState -> ServerMessage -> State Metrics ClientMessage
logic track@(Track{..}) myId (AnalyzedTrack{..}) = respond where
    respond st (CarPositions _ gameTick) = do
        Metrics{..} <- get

        let (CarState{..}) = car myId st

            absAngle = abs carAngle

            -- taikavakiot: keimola
            frictionalForce = 0.324
            maxDesiredAngle = 59
            frictionalConstant = 192
            topSpeed = 10.0
            acceleration = 0.02

            curveRemaining
                | pieceAngle carCurrentPiece == 0 = 0
                | otherwise = Angle $ (abs $ toDouble $ remainingCurveAngle track') - carPieceAngleDist where
                    carPieceAngleDist = toDouble carPieceDist / toDouble carLaneRadius * 180 / pi

            maxSpeedVal
                | needToBrake = brakeTargetSpeed
                | otherwise   = fromDouble $ carAngleCoeff * maxSpeedForRemainingCurve * carAngleAbsDeltaCoeff where
                    carAngleCoeff
                        | negativeCarAngle = 1
                        | otherwise        = cos $ toRadians $ carAngle * 2 / 3

                    carAngleAbsDeltaCoeff
                        | negativeCarAngle = cos $ toRadians $ carAngleAbsDelta * 4.5
                        | otherwise        = cos $ toRadians $ carAngleAbsDelta * 9

                    negativeCarAngle          = signum carAngle /= signum (pieceAngle carCurrentPiece)
                    maxSpeedForRemainingCurve = (toDouble $ maxSpeed carLaneRadius curveRemaining maxDesiredAngle frictionalForce frictionalConstant)
                    toRadians dec             = (toDouble dec) * pi / 180

            avgBrakeCoef = 0.98
                -- | brakeSamples < 5 = 0.98
                -- | otherwise        = brakeCoefficient / fromIntegral brakeSamples


            piecesFromIndex startIndex = step (succ startIndex) where
                step i
                    | i == startIndex = []
                    | i > maxIndex = step minIndex
                    | otherwise = i : step (succ i)

                (minIndex, maxIndex) = bounds trackPieces

            maxSpeedFor :: PieceIndex -> LaneIndex -> Velocity
            maxSpeedFor i l = (pieceMaxSpeed ! i) maxDesiredAngle (frictionalForce) frictionalConstant l

            brakeDistanceFor :: Velocity -> Distance
            brakeDistanceFor v =
                fromDouble $ avgBrakeCoef * toDouble (v * 1.1 - carSpeed) / (avgBrakeCoef - 1)

            (needToBrake, brakeTargetSpeed) = case brakeCurve of
                [] -> (False, 99)
                _  -> (True, minimum brakeCurve)

            brakeCurve = map snd . filter fst . map (\(b, ms, _) -> (b, ms)) . dropWhile (\(_, _, p) -> belongsToSameSection carCurrentPiece p) $ zipWith calcBrake indices distances where
                indices   = piecesFromIndex carPieceIndex
                distances = scanl (+) carNextPieceDist . map calcLength $ indices
                calcLength i = pieceLength (lanes ! laneIndex i) (trackPieces ! i)
                calcBrake i d =
                    let ms = maxSpeedFor i (laneIndex i)
                        bd = brakeDistanceFor ms
                        p  = trackPieces ! i
                    in (bd > d, ms, p)
                laneIndex i
                    | switchExists && i `isBetween` (nextSwitchIndex, carPieceIndex) = laneAfterSwitch
                    | otherwise = carLaneIndex




            throttle
                | braking = 0
                | carLaneRadius == 0 = 1.0
                | carSpeed < maxSpeedVal = unsafeConvert $ min 1 ((maxSpeedVal + (maxSpeedVal - carSpeed) / 0.02) / 10)
                | carSpeed > maxSpeedVal = unsafeConvert $ max 0 ((maxSpeedVal + (maxSpeedVal - carSpeed) / 0.02) / 10)
                | otherwise = 1 --min 1 $ curveCoeff -- prevThrottle -- max 0 $ fromDouble $ (1 - (toDouble carSpeed - maxSpeed)*15/maxSpeed)

            turboAvailable = checkTurbo turboTicksLeft where
                checkTurbo (GameTicks ticks) = ticks > 0

            shouldUseTurbo = safeToTurbo carPieceIndex where
                safeToTurbo = ((==) (fst safestTurboPlace))

            --debugInfo = printf "Tick: %5s | PieceIdx: %i | Spd: %5.2f/%5.2f | A: %6.2f | r: %6.2f | TA: %6.2f | Throttle: %5.2f | Brake: %s"
            --    (show gameTick)
            --    (fromEnum carPieceIndex)
            --    (toDouble carSpeed)
            --    (toDouble maxSpeed)
            --    (toDouble carAngle)
            --    (toDouble carLaneRadius)
            --    (toDouble $ currentCurveAngle track carPieceIndex)
            --    (toDouble throttle)
            --    (show needToBrake)

            -- Time	Piece	Radius	Angle	DriftAngle	Throttle	Acceleration	Velocity	MaxSpeed
            debugInfo = printf "%7.3f\t%3d\t%6.2f\t%6.2f\t%6.2f\t%6.2f\t%6.2f\t%6.2f\t%6.2f"
                (let (GameTick tick) = gameTick in fromIntegral tick / 60.0 :: Float)
                (fromEnum  carPieceIndex)
                (toDouble  (signum $ pieceAngle carCurrentPiece) * (toDouble carLaneRadius))
                (toDouble  curveRemaining)
                (toDouble  carAngle)
                (toDouble  throttle)
                (toDouble  carAcceleration)
                (toDouble  carSpeed)
                (toDouble  maxSpeedVal)

            -- Calculate current brake multiplier
            brakeMultiplier = (carSpeed + carAcceleration) / carSpeed

            -- Lane switching logic
            (switchExists, ~(Just (nextSwitchIndex, nextSwitch))) = go $ piecesFromIndex carPieceIndex where
                go (i:is) = case switchPieces ! i of
                    Nothing -> go is
                    Just sw -> (True, Just (i, sw))
                go _ = (False, Nothing)

            bestLaneIndex = fst
                $ minimumBy (comparing $ \(i,l) -> (l, i /= carLaneIndex))
                $ assocs
                $ laneLengths nextSwitch

            switchDirection
                | not switchExists = Nothing
                | bestLaneIndex < carLaneIndex = Just DLeft
                | bestLaneIndex > carLaneIndex = Just DRight
                | otherwise = Nothing

            laneAfterSwitch = case switchDirection of
                Nothing     -> carLaneIndex
                Just DLeft  -> pred carLaneIndex
                Just DRight -> succ carLaneIndex

            needToSwitch = isJust switchDirection && laneSwitch /= switchDirection



        modify $ \m -> m { prevThrottle = throttle, prevAngle = carAngle, prevPieceIndex = carPieceIndex }

        when (needToBrake) $
            modify $ \m -> m { braking = True }

        when (not $ belongsToSameSection (trackPieces ! prevPieceIndex) carCurrentPiece) $
            modify $ \m -> m { braking = False }

        when (absAngle > abs prevAngle && absAngle > angleThreshold) $
            modify $ \m -> m { currentStraight = (carPieceIndex, 0) }

        when (prevPieceIndex /= carPieceIndex) $
            modify $ \m -> m { currentStraight = (fst currentStraight, succ $ snd currentStraight) }

        when (snd currentStraight > snd safestTurboPlace) $
            modify $ \m -> m { safestTurboPlace = currentStraight }

        when (prevThrottle == 0 && carSpeed > 2 && brakeMultiplier < 1.0) $
            modify $ \m -> m { brakeCoefficient = brakeCoefficient + toDouble brakeMultiplier, brakeSamples = brakeSamples + 1 }

        when (hasSwitch carCurrentPiece) $
            modify $ \m -> m { laneSwitch = Nothing }

        when (turboAvailable && shouldUseTurbo) $
            modify $ \m -> m { turboTicksLeft = (GameTicks 0) }

        trace debugInfo $ case switchDirection of
            Just dir | needToSwitch && throttle == prevThrottle -> do
                modify $ \m -> m { laneSwitch = switchDirection }
                return $ SwitchLane dir
            _ | turboAvailable && shouldUseTurbo && carLaneRadius == 0 -> do
                return $ TurboMessage $ TurboText "WROOM WROOM!!!!"
            _ -> return $ ThrottleMessage throttle

    respond st (TurboAvailable gameTicks turboFactor) = do
        Metrics{..} <- get
        modify $ \m -> m { turboFactor = turboFactor, turboTicksLeft = gameTicks }
        return Ping

    respond _ msg  = trace ("Ignored message:" ++ show msg) $ return Ping
