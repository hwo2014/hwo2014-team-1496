
module Agapio.AI.Metrics where

import Data.Map (Map)
import qualified Data.Map as Map

import Agapio.Types

-- Metrics contain the information that the AI gathers during the race about
-- physics, road and car properties.

data Metrics = Metrics
    { brakeCoefficient :: Double
    , brakeSamples     :: Int

    , prevThrottle     :: Throttle
    , prevAngle        :: Angle
    , prevPieceIndex   :: PieceIndex

    , laneSwitch       :: Maybe Direction

    , pieceMetrics     :: Map PieceIndex PieceMetrics
    , angleThreshold   :: Angle
    , turboFactor      :: TurboFactor
    , turboTicksLeft   :: GameTicks
    , currentStraight  :: (PieceIndex, TrackPieces)
    , safestTurboPlace :: (PieceIndex, TrackPieces)
    , braking          :: Bool
    }

-- TODO: need lane specific metrics
data PieceMetrics = PieceMetrics
    { maxEnterSpeed :: Velocity
    , maxExitAngle  :: Angle
    , crashLimit    :: Velocity
    }

initPieceMetrics :: PieceMetrics
initPieceMetrics = PieceMetrics
    { maxEnterSpeed = 0
    , maxExitAngle  = 0
    , crashLimit    = 100
    }

initMetrics :: Metrics
initMetrics = Metrics
    { brakeCoefficient = 0
    , brakeSamples     = 0
    , prevThrottle     = 0
    , prevAngle        = 0
    , angleThreshold   = 20
    , prevPieceIndex   = PieceIndex 0
    , laneSwitch       = Nothing
    , pieceMetrics     = Map.empty
    , turboFactor      = TurboFactor 0
    , turboTicksLeft   = GameTicks 0
    , currentStraight  = (PieceIndex 0, 0)
    , safestTurboPlace = (PieceIndex 0, 0)
    , braking          = False
    }
