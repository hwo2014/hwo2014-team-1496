{-# LANGUAGE RecordWildCards #-}

module Agapio.AI.State where

import Data.Maybe (fromJust)
import Data.List (foldl')
import Data.HashMap.Strict (HashMap)
import Data.Array ((!), bounds, elems, listArray)
import qualified Data.HashMap.Strict as Hash

import Agapio.Types
import qualified Agapio.Models as M

data GameState = GameState
    { carStates :: HashMap CarId CarState
    }

data CarState = CarState
    { carCurrentPiece  :: M.Piece
    , carPieceIndex    :: PieceIndex
    , carNextPiece     :: M.Piece
    , carPieceLength   :: Length
    , carLaneIndex     :: LaneIndex
    , carLaneRadius    :: Radius
    , carNextRadius    :: Radius
    , carPieceDist     :: Distance
    , carNextPieceDist :: Distance
    , carSpeed         :: Velocity
    , carAcceleration  :: Acceleration
    , carAngle         :: Angle
    , carAngleVelocity :: Angle
    , carAngleAbsDelta :: Angle
    , track'           :: [M.Piece]
    }

car :: CarId -> GameState -> CarState
car cid (GameState{..}) = fromJust $ Hash.lookup cid carStates

initState :: M.Track -> [M.Car] -> GameState
initState (M.Track{..}) = GameState . Hash.fromList . map initCar where
    initCar (M.Car{..}) = (carId, st)
    st = CarState
        { carPieceIndex    = PieceIndex 0
        , carCurrentPiece  = trackPieces ! PieceIndex 0
        , carNextPiece     = trackPieces ! PieceIndex 1
        , carLaneIndex     = LaneIndex 0
        , carPieceLength   = 0
        , carLaneRadius    = 0
        , carNextRadius    = 0
        , carPieceDist     = 0
        , carNextPieceDist = 0
        , carSpeed         = 0
        , carAcceleration  = 0
        , carAngle         = 0
        , carAngleVelocity = 0
        , carAngleAbsDelta = 0
        , track'           = []
        }

updateState :: M.Track -> [(CarId, M.CarPosition)] -> GameState -> GameState
updateState track@(M.Track{..}) = update' where

    update' cars prev@(GameState{..}) = prev { carStates = updatedCars } where

        updatedCars = foldl' (\h (k,v) -> Hash.adjust (adjustCar v) k h) carStates cars
        adjustCar new prev = CarState
            { carCurrentPiece  = curPiece
            , carNextPiece     = nextPiece
            , carPieceLength   = curPieceLength
            , carPieceDist     = curPos
            , carLaneIndex     = curLane
            , carLaneRadius    = laneRadius curLane curPiece
            , carNextPieceDist = curPieceLength - inPieceDist
            , carNextRadius    = laneRadius curLane nextPiece
            , carSpeed         = let a = acceleration in if abs a > 0.6 then carSpeed prev else speed
            , carAcceleration  = let a = acceleration in if abs a > 0.6 then carAcceleration prev else a
            , carAngle         = angle
            , carAngleVelocity = angle - (carAngle prev)
            , carAngleAbsDelta = abs angle - abs (carAngle prev)
            , carPieceIndex    = curIndex
            , track'           = M.trackPieces' track curIndex
            } where
                position    = M.carPosition new
                curIndex    = M.pieceIndex position
                curPiece    = trackPieces ! curIndex
                curPos      = inPieceDist
                inPieceDist = M.pieceDistance position
                curLap      = M.carLap new
                speed       = curPos - (carPieceDist prev) + compDist
                acceleration = speed - (carSpeed prev)
                angle       = M.carAngle new
                nextPiece   = trackPieces ! (wrapSucc maxIndex curIndex)

                curLane        = M.maxLane position
                curPieceLength = pieceLength curLane curPiece

                -- Correction to speed calculation when we wrap around the track
                compDist
                    | curIndex /= (carPieceIndex prev) = carPieceLength prev
                    | otherwise                        = 0

                -- TODO: correct velocity for the first Lap 0 update

    (_, maxIndex)    = bounds trackPieces
    -- totalTrackLength = foldl' (+) 0 $ pieceLengthsLst
    -- pieceLengths     = listArray (bounds trackPieces) pieceLengthsLst
    -- pieceLengthsLst   = map M.pieceLength $ elems trackPieces

    laneRadius lane piece = case piece of
        M.Straight _ _ -> 0
        M.Curve radius angle _ -> radius' where
            radius'
                | angle < 0 = radius + lanes ! lane
                | otherwise = radius - lanes ! lane

    pieceLength lane piece = case piece of
        M.Straight len _       -> len
        M.Curve radius angle _ -> fromDouble pi * radius' * abs (unsafeConvert angle) / 180.0 where
            radius'
                | angle < 0 = radius + lanes ! lane
                | otherwise = radius - lanes ! lane

