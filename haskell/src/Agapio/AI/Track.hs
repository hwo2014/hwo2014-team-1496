{-# LANGUAGE RecordWildCards #-}

-- Track analysis

module Agapio.AI.Track where

import Control.Applicative
import Data.Array

import Agapio.Types
import Agapio.Models

import Debug.Trace
import Text.Printf

type PieceData  = Array PieceIndex SpeedFunction
type SwitchData = Array PieceIndex (Maybe Switch)

type FrictionalForce = Double
type FrictionalConstant = Double

type SpeedFunction = Angle -> FrictionalForce -> FrictionalConstant -> LaneIndex -> Velocity

data Switch = Switch
    { laneLengths :: Array LaneIndex Distance  -- Distance of each lane between this and the next switch point
    }

data AnalyzedTrack = AnalyzedTrack
    { pieceMaxSpeed :: PieceData
    , switchPieces  :: SwitchData
    }

analyzeTrack :: Track -> AnalyzedTrack
analyzeTrack track = AnalyzedTrack (findCurves track) (analyzeLanes track)

analyzeLanes :: Track -> Array PieceIndex (Maybe Switch)
analyzeLanes (Track{..}) = accumArray (<|>) Nothing bds $ step (PieceIndex 0) where

    step i
        | i > maxIndex    = []
        | hasSwitch piece = let (sw, rest) = calcLanes i in (i, sw) : rest
        | otherwise       = step $ succ i
        where
            piece = trackPieces ! i

    calcLanes = go (repeat 0) . succ where
        go dist i
            | i > maxIndex    = (sw dist, [])
            | hasSwitch piece = (sw dist', step i)
            | otherwise       = go dist' (succ i)
            where
                piece = trackPieces ! i
                laneDistances = map (\l -> pieceLength l piece) (elems lanes)
                dist' = zipWith (+) dist laneDistances
                sw = Just . Switch . listArray (bounds lanes)

    bds = bounds trackPieces
    (minIndex, maxIndex) = bds

findCurves :: Track -> PieceData
findCurves (Track{..}) = listArray (bounds trackPieces) $ initStep (PieceIndex 0) where
    initStep i | i > maxIndex = []
    initStep i = case piece of
        Straight {}     -> noLimit : (initStep $ succ i)
        Curve rad ang _ -> snd $ curveStep i i rad (signum ang) 0
        where piece = trackPieces ! i

    curveStep begin i rad _ totalAngle | i > maxIndex = (func rad totalAngle, [])
    curveStep begin i rad angleSign totalAngle = case piece of
        Curve rad' ang' _ | rad == rad' &&  signum ang' == angleSign ->
            let (f, rest) = curveStep begin (succ i) rad angleSign (totalAngle + ang')
            in  (f, f : rest)
        _ -> (func rad totalAngle, initStep i)
        where piece = trackPieces ! i

    (_, maxIndex) = bounds trackPieces

    noLimit _ _ _ _ = 99.0

    func rad totalAngle angleLimit ff fC laneIndex
        = maxSpeed r totalAngle angleLimit ff fC where
            r = rad - unsafeConvert (signum totalAngle) * lanes ! laneIndex

maxSpeed :: Radius -> Angle -> Angle -> FrictionalForce -> FrictionalConstant -> Velocity
maxSpeed rad totalAngle angleLimit ff fC =  fromDouble $ sqrt $ r * toDouble angleLimit / (fC * (1 - curveCosine)) + v0 where
    r           = toDouble $ rad
    v0          = r * ff
    curveCosine = cos $ toDouble (min 180 $ abs totalAngle + 35) * pi / 180
