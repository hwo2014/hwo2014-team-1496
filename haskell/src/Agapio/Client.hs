
-- Socket client
module Agapio.Client where

import Agapio.Client.Monad
import Agapio.Types
import Agapio.Models
import Agapio.Protocol

joinGame :: Name -> Key -> Client (CarId, Track, [Car])
joinGame name key = do
    sendMessage $ Join name key
    ConfirmJoin <- fromServer
    YourCar car <- fromServer
    GameInit track cars <- fromServer
    return (car, track, cars)

joinRace :: Name -> Key -> TrackName -> CarCount -> Password -> Client (CarId, Track, [Car])
joinRace name key trackName carCount password = do
    sendMessage $ JoinRace name key trackName carCount password
    ConfirmJoin <- fromServer
    YourCar car <- fromServer
    GameInit track cars <- fromServer
    return (car, track, cars)
