{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

-- Socket client
module Agapio.Client.Monad where

import Control.Applicative
import Control.Monad
import Control.Arrow (Kleisli(..))

import System.IO (Handle)
import Data.Aeson (eitherDecode', encode)
import Data.Function (fix)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BL

import Agapio.Protocol

newtype Client a = Client { runClient :: Handle -> IO a }

debug :: Show s => String -> s -> Client ()
debug prefix val = Client $ const $ debugIO prefix val

debugIO :: Show s => String -> s -> IO ()
debugIO prefix val = putStrLn $ concat [prefix, " (", show val, ")"]

sendMessage :: ClientMessage -> Client ()
sendMessage msg = Client $ \h -> do
    BL.hPutStr h $ encode msg
    BL.hPutStr h "\n"

fromServer :: Client ServerMessage
fromServer = Client $ \h -> fix $ \loop -> do
    bytes <- BS.hGetLine h
    case eitherDecode' (BL.fromStrict bytes) of
        Left err  -> debugIO ("error parsing json: " ++ err) bytes >> loop
        Right val -> return val


instance Functor Client where
    fmap f (Client c) = Client $ fmap f . c
    {-# INLINE fmap #-}

instance Applicative Client where
    pure = Client . const . pure
    Client f <*> Client x = Client $ \h -> f h <*> x h
    {-# INLINE pure #-}
    {-# INLINE (<*>) #-}

instance Monad Client where
    return = pure
    Client x >>= f = Client $ \h -> x h >>= flip runClient h . f
    {-# INLINE return #-}
    {-# INLINE (>>=) #-}
