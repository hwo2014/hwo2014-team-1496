{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

-- Record types for the game's data models
module Agapio.Models where

import Control.Applicative

import Data.Aeson (FromJSON(..), (.:), (.:?), Value(..))
import Data.Aeson.Types (Parser, modifyFailure)
import Data.Array (Array, listArray, Ix, bounds, (!), assocs)

import qualified Data.Vector as V

import Agapio.Types

data Track = Track
    { trackId     :: !TrackId
    , trackPieces :: !(Array PieceIndex Piece)
    , lanes       :: !(Array LaneIndex Distance)
    }
    deriving Show

data Car = Car
    { carId         :: !CarId
    , carDimensions :: !Dimensions
    }
    deriving Show

data CarPosition = CarPosition
    { carAngle    :: !Angle
    , carPosition :: !PiecePosition
    , carLap      :: !Lap
    }
    deriving Show

data Piece
    = Straight !Length !HasSwitch
    | Curve    !Radius !Angle !HasSwitch
    deriving Show

data Dimensions = Dimensions
    { carLength  :: !Length
    , carWidth   :: !Width
    , carFlagPos :: !Distance
    }
    deriving Show

data PiecePosition = PiecePosition
    { pieceIndex    :: !PieceIndex
    , pieceDistance :: !Distance
    , minLane       :: !LaneIndex
    , maxLane       :: !LaneIndex
    }
    deriving Show

array :: (Ix i, Enum i, FromJSON i, FromJSON v) => Value -> Parser (Array i v)
array (Array v) = info "array:" $ listArray (toEnum 0, toEnum (V.length v - 1))
    <$> mapM parseJSON (V.toList v)
array _ = fail "expected an array"

newtype Lane = Lane { fromLane :: Distance }

instance FromJSON Lane where
    parseJSON (Object o) = info "Lane: " $ Lane <$> o.:"distanceFromCenter"
    parseJSON _ = fail "expected lane object"

instance FromJSON Track where
    parseJSON (Object o) = info "Track:" $ Track
        <$> o.:"id"
        <*> (array =<< o.:"pieces")
        <*> ((fmap.fmap) fromLane . array =<< o.:"lanes")
    parseJSON _ = fail "Expected track object"

instance FromJSON Piece where
    parseJSON (Object o) = info "Piece:" $ do
        l  <- o .:? "length"
        sw <- o .:? "switch"
        let switch = sw == Just True
        case l of
            Just len -> return $ Straight len switch
            Nothing  -> Curve <$> o.:"radius" <*> o.:"angle" <*> pure switch

    parseJSON _ = fail "expected a piece object"

instance FromJSON Car where
    parseJSON (Object o) = info "Car:" $ Car <$> o.:"id" <*> o.:"dimensions"
    parseJSON _ = fail "expected a car object"

instance FromJSON Dimensions where
    parseJSON (Object o) = info "Dimensions:" $ Dimensions <$> o.:"length" <*> o.:"width" <*> o.:"guideFlagPosition"
    parseJSON _ = fail "expected car dimensions"

instance FromJSON CarPosition where
    parseJSON (Object o) = info "CarPosition:" $ do
        pp@(Object po) <- o.:"piecePosition"
        CarPosition <$> o.:"angle" <*> parseJSON pp <*> po.:"lap"
    parseJSON _ = fail "expected car position object"

instance FromJSON PiecePosition where
    parseJSON (Object o) = info "PiecePosition:" $ PiecePosition
        <$> o.:"pieceIndex"
        <*> o.:"inPieceDistance"
        <*> (o.:"lane" >>= (.:"startLaneIndex"))
        <*> (o.:"lane" >>= (.:"endLaneIndex"))

info :: String -> Parser a -> Parser a
info txt = modifyFailure (txt ++)

hasSwitch :: Piece -> Bool
hasSwitch (Straight _ sw) = sw
hasSwitch (Curve _ _ sw)  = sw

pieceAngle :: Piece -> Angle
pieceAngle (Straight _ _) = 0
pieceAngle (Curve _ a _)  = a

pieceRadius :: Piece -> Length
pieceRadius (Straight _ _) = 0
pieceRadius (Curve r _ _) = r


pieceLength :: Distance -> Piece -> Length
pieceLength lane piece = case piece of
    Straight len _       -> len
    Curve radius angle _ -> fromDouble pi * radius' * abs (unsafeConvert angle) / 180.0 where
        radius'
            | angle < 0 = radius + lane
            | otherwise = radius - lane

-- Track pieces starting from current position
trackPieces' :: Track -> PieceIndex -> [Piece]
trackPieces' (Track{..}) (PieceIndex i) = ((take $ length ps - i) . (drop i) $ ps) ++ (take i ps) where
    ps = map snd $ assocs trackPieces

belongsToSameSection :: Piece -> Piece -> Bool
belongsToSameSection (Curve r a _) (Curve r' a' _) = r == r' && signum a == signum a'
belongsToSameSection (Straight {}) (Straight {}) = True
belongsToSameSection _ _ = False

totalCurveAngle :: [Piece] -> Angle
totalCurveAngle pieces = remainingCurveAngle pieces + remainingCurveAngle (reverse pieces)

remainingCurveAngle :: [Piece] -> Angle
remainingCurveAngle pieces = sum . (map pieceAngle) $ curvePieces where
    curvePieces  = takeWhile (belongsToSameSection currentPiece) pieces
    currentPiece = head pieces

nextCurve :: Track -> PieceIndex -> (PieceIndex, Radius, Angle)
nextCurve (Track{..}) start = step start where

    step i = case piece of
        Curve r a _ | a /= startAngle -> (i', r, a)
        Straight _ _ -> step2 i'
        _ -> step i'
        where
            piece = trackPieces ! i'
            i' = wrapSucc (snd $ bounds trackPieces) i

    step2 i = case piece of
        Curve r a _ -> (i', r, a)
        Straight _ _ -> step2 i'
        where
            piece = trackPieces ! i'
            i' = wrapSucc (snd $ bounds trackPieces) i

    startAngle = pieceAngle $ trackPieces ! start

nextSwitch :: Track -> PieceIndex -> PieceIndex
nextSwitch  (Track{..}) start = step start where

    step i
        | hasSwitch piece = i'
        | otherwise       = step i'
        where
            piece = trackPieces ! i'
            i' = wrapSucc (snd $ bounds trackPieces) i

isBetween :: Ord o => o -> (o, o) -> Bool
isBetween p (a, b)
    | b < a && (p > a || p < b) = True
    | p > a && p < b = True
    | otherwise = False
