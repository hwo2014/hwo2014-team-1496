{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

-- Data types describing the messaging protocol
module Agapio.Protocol where

import Control.Applicative
import Control.Monad

import Data.String (IsString)
import Data.Aeson
import Data.Aeson.Types (Parser, Pair)
import Data.Text  (Text)

import Agapio.Models
import Agapio.Types


data ClientMessage
    = Join            !Name !Key
    | JoinRace        !Name !Key !TrackName !CarCount !Password
    | ThrottleMessage !Throttle
    | TurboMessage    !TurboText
    | SwitchLane      !Direction
    | Ping
    deriving Show

data ServerMessage
    = ConfirmJoin
    | YourCar        !CarId
    | GameInit       !Track ![Car] -- !RaceSession
    | GameStart
    | CarPositions   ![(CarId, CarPosition)] GameTick
    | GameEnd        !BestLaps
    | Crash          !CarId
    | Spawn          !CarId
    | LapFinished    !CarId !LapTime !RaceTime -- !Ranking
    | Disqualified   !CarId !Reason
    | Finish         !CarId
    | TurboAvailable !GameTicks !TurboFactor
    | Unknown        !MessageType !Value
    deriving Show

newtype MessageType = MessageType Text deriving (Show, ToJSON, FromJSON, IsString, Eq)
type    MessageData = Value

msgToJSON :: ToJSON d => MessageType -> d -> Value
msgToJSON mt md = object ["msgType" .= mt, "data" .= md]

msgParseJSON :: FromJSON d => (Object -> (MessageType, MessageData) -> Parser d) -> Value -> Parser d
msgParseJSON f (Object o) = join $ curry (f o) <$> o .: "msgType" <*> o .: "data"
msgParseJSON _ _          = fail "Invalid message from server, expected a json object"

newtype CarIdWith d = CarIdWith { toPair :: (CarId, d) }

instance ToJSON ClientMessage where
    toJSON (Join name key) = msgToJSON "join"
        $ object ["name" .= name, "key" .= key]

    toJSON (JoinRace name key trackName carCount password) = msgToJSON "joinRace"
        $ object ["botId" .= object ["name" .= name, "key" .= key],
                  "trackName" .= trackName, "carCount" .= carCount, "password" .= password]

    toJSON (ThrottleMessage t) = msgToJSON "throttle" t

    toJSON (SwitchLane dir) = msgToJSON "switchLane" dir

    toJSON (TurboMessage msg) = msgToJSON "turbo" msg

    toJSON Ping = msgToJSON "ping" Null

instance FromJSON ServerMessage where
    parseJSON = msgParseJSON $ \org p -> case p of
        ("join", msgData) -> return ConfirmJoin
        ("joinRace", msgData) -> return ConfirmJoin
        ("yourCar", msgData) -> YourCar <$> parseJSON msgData
        ("gameInit", Object o) -> do
            msgData <- o .: "race"
            GameInit <$> msgData.:"track" <*> msgData.:"cars"
        ("gameStart", Null) -> return GameStart
        ("carPositions", carList) -> CarPositions <$> (fmap.map) toPair (parseJSON carList) <*> org.:"gameTick"
        ("gameEnd", msgData) -> GameEnd . map toPair <$> parseJSON msgData
        ("crash", msgData) -> Crash <$> parseJSON msgData
        ("spawn", msgData) -> Spawn <$> parseJSON msgData
        ("lapFinished", Object o) -> LapFinished <$> o.:"car" <*> o.:"lapTime" <*> o.:"raceTime"
        ("dnf", Object o) -> Disqualified <$> o.:"car" <*> o.:"reason"
        ("finish", msgData) -> Finish <$> parseJSON msgData
        ("turboAvailable", Object o) -> TurboAvailable <$> o.:"turboDurationTicks" <*> o.:"turboFactor"
        (msgType, js) -> return $ Unknown msgType js

instance FromJSON d => FromJSON (CarIdWith d) where
    parseJSON js@(Object o) = (.) CarIdWith . (,) <$> o.:"id" <*> parseJSON js
    parseJSON _ = fail "expected car position object"

