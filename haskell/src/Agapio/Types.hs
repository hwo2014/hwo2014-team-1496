{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

-- Basic, domain specific primitive types
module Agapio.Types where

import Control.Applicative

import Data.Array (Ix)
import Data.Aeson
import Data.Hashable (Hashable(..))
import Data.Text  (Text)

newtype Name       = Name Text deriving (Show, ToJSON, FromJSON, Eq, Hashable)
newtype Key        = Key  Text deriving (Show, ToJSON)
newtype TrackName  = TrackName Text deriving (Show, ToJSON)
newtype CarCount   = CarCount Int deriving (Show, ToJSON)
newtype Password   = Password Text deriving (Show, ToJSON)
newtype Color      = Color Text deriving (Show, FromJSON)
newtype Throttle   = Throttle Double deriving (Show, Eq, Ord, Num, Fractional, ToJSON)
data    Direction  = DLeft | DRight deriving (Show, Eq)
data    CarId      = CarId !Name !Color deriving Show
type    BestLaps   = [(CarId, LapTime)]
newtype GameTick   = GameTick Int deriving (Show, FromJSON)
newtype Millis     = Millis Int deriving (Show, FromJSON)
data    RaceTime   = RaceTime !Laps !GameTick !Millis deriving Show
data    LapTime    = LapTime  !Lap  !GameTick !Millis deriving Show
newtype Lap        = Lap Int deriving (Show, Eq, Ord, FromJSON)
newtype Laps       = Laps Int deriving (Show, FromJSON)
newtype Reason     = Reason Text deriving (Show, FromJSON)
newtype Angle      = Angle Double deriving (Show, Eq, Ord, Num, Fractional, FromJSON)
newtype TrackId    = TrackId Text deriving (Show, Eq, FromJSON)
newtype PieceIndex = PieceIndex Int deriving (Show, Eq, Ord, Enum, Ix, FromJSON)
newtype LaneIndex  = LaneIndex  Int deriving (Show, Eq, Ord, Enum, Ix, FromJSON)
newtype Length     = Length Double deriving (Show, Eq, Ord, Num, Fractional, FromJSON)
type    HasSwitch  = Bool
type    Radius     = Length
type    Width      = Length
type    Distance   = Length
data    Turbo      = Turbo !GameTicks !TurboFactor deriving Show
newtype GameTicks  = GameTicks Int deriving (Show, Eq, Ord, FromJSON)
newtype TurboFactor = TurboFactor Float deriving (Show, Eq, Ord, FromJSON)
newtype TurboText  = TurboText Text deriving (Show, ToJSON)
type    TrackPieces = Int
-- Simplify formulas at the cost of type safety
type Velocity     = Length
type Acceleration = Length

{-
newtype Velocity     = Velocity Double     deriving (Show, Eq, Ord, Num)
newtype Acceleration = Acceleration Double deriving (Show, Eq, Ord, Num)
-}

instance ToJSON Direction where
    toJSON DLeft  = toJSON ("Left"  :: Text)
    toJSON DRight = toJSON ("Right" :: Text)

instance FromJSON CarId where
    parseJSON (Object o) = CarId <$> o.:"name" <*> o.:"color"
    parseJSON _          = fail "Expected an object with car name and color"

instance FromJSON RaceTime where
    parseJSON (Object o) = RaceTime <$> o.:"laps" <*> o.:"ticks" <*> o.:"millis"
    parseJSON _          = fail "Expected race time object"

instance FromJSON LapTime where
    parseJSON (Object o) = LapTime <$> o.:"lap" <*> o.:"ticks" <*> o.:"millis"
    parseJSON _          = fail "Expected lap time object"

instance Eq CarId where
    (CarId a _) == (CarId b _) = a == b

instance Hashable CarId where
    hashWithSalt salt (CarId name _) = hashWithSalt salt name
    hash (CarId name _) = hash name

class IsDouble d where
    fromDouble :: Double -> d
    toDouble   :: d -> Double

unsafeConvert :: (IsDouble a, IsDouble b) => a -> b
unsafeConvert = fromDouble . toDouble

-- Give the successor of index which is wrapped around to zero after the
-- given maxIndex.
wrapSucc :: PieceIndex -> PieceIndex -> PieceIndex
wrapSucc (PieceIndex maxIndex) (PieceIndex currIndex) = PieceIndex (succ currIndex `rem` succ maxIndex)

instance IsDouble Angle where
    fromDouble a = Angle a
    toDouble (Angle a) = a

instance IsDouble Length where
    fromDouble l = Length l
    toDouble (Length l) = l

instance IsDouble Throttle where
    fromDouble l = Throttle l
    toDouble (Throttle l) = l
