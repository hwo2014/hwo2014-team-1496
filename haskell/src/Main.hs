{-# LANGUAGE OverloadedStrings #-}

import Network(connectTo, PortID(..))
import System.IO
import System.Environment (getArgs)
import System.Exit (exitFailure)
import qualified Data.Text as T

import Agapio.AI
import Agapio.AI.Logic
import Agapio.AI.Track (analyzeTrack)
import Agapio.AI.State (initState)
import Agapio.AI.Metrics (initMetrics)
import Agapio.Client
import Agapio.Client.Monad (runClient)
import Agapio.Protocol
import Agapio.Types

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main :: IO ()
main = do
    args <- getArgs
    case args of
        [server, port, botname, botkey] -> do
            run server port $ joinGame (Name $ T.pack botname) (Key $ T.pack botkey)
        [server, port, botname, botkey, trackname, carcount] -> do
            run server port $ joinRace (Name $ T.pack botname) (Key $ T.pack botkey) (TrackName $ T.pack trackname) (CarCount $ read carcount) (Password "NA")
        _ -> do
            putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey> [<trackname> <carcount>]"
            exitFailure

run server port command = do
    h <- connectToServer server port
    hSetBuffering h LineBuffering
    runClient (do
        (carId, track, cars) <- command
        let analyzed = analyzeTrack track
        statefulHandler track carId analyzed initMetrics (initState track cars) logic
        ) h
